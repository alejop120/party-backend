Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  resources :ratings
  resources :staffs
  resources :services do
    resources :user_services
  end
  devise_for :users

  root to: 'dashboards#dashboard_admin', constraints: RoleConstraint.new(:admin), as: :authenticated_admin #matches this route when the current user is an super admin
  root 'dashboards#dashboard'

  ## API ##
  namespace :api, defaults: { format: "json" } do
  	namespace :v1 do
  		post 'users', to: 'users#register'
  		post 'users/login', to: 'users#login'
      get 'services', to: 'services#index'
      post 'services', to: 'services#new_request'
      post 'ratings', to: 'ratings#new_rating'
  	end
  end
end
