class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :name, null: false
      t.string :description, null: false
  	  t.boolean :is_active, default: true, null: false

      t.timestamps
    end
  end
end
