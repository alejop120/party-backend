class AddFieldsToUser < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :names, :string, null: false
  	add_column :users, :surnames, :string, null: false
  	add_column :users, :role, :integer, null: false, default: 2
  	add_column :users, :type_register, :integer, null: false, default: 0
  	add_column :users, :auth_token, :string
  	add_index :users, :auth_token, unique: true
  	add_column :users, :is_active, :boolean, default: true, null: false
  end
end
