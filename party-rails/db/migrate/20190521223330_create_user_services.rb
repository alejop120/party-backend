class CreateUserServices < ActiveRecord::Migration[5.2]
  def change
    create_table :user_services do |t|
      t.references :user, foreign_key: true, null: false, index: true
      t.references :service, foreign_key: true, null: false

      t.timestamps
    end
  end
end
