# spec/integration/users_spec.rb
describe 'Users API' do

  path '/users' do

    post 'Creates a user' do
      tags 'Users'
      consumes 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          email: { type: :string },
          names: { type: :string },
          surnames: { type: :string },
          type_register: { type: :string },
          password: { type: :string },
          password_confirmation: { type: :string }
        },
        required: [ 'email', 'names', 'surnames', 'type_register', 'password', 'password_confirmation' ]
      }

      response '201', 'user created' do
        examples 'application/json' => {
          "success": true,
          "status": 201,
          "message": "Bienvenido",
          "user": {
            "id": 5,
            "email": "test@gmail.com",
            "names": "Jose Alejandro",
            "surnames": "Peña Gonzalez",
            "role": "user",
            "type_register": "app",
            "auth_token": "3989617",
            "is_active": true
          }
        }
        # let(:user) { { names: 'Jose Alejandro', email: 'test@gmail.com' } }
        run_test!
      end

      response '422', 'Email already exists' do
        examples 'application/json' => {
          "success": false,
          "status": 422,
          "message": "El correo electrónico ya está registrado"
        }
        # let(:user) { { names: 'Jose Alejandro' } }
        run_test!
      end
    end
  end

  path '/users/login' do
    post 'Login user' do
      tags 'Users'
      consumes 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          email: { type: :string },
          password: { type: :string }
        },
        required: [ 'email', 'password' ]
      }

      response '200', 'login success' do
        examples 'application/json' => {
          "success": true,
          "status": 201,
          "message": "Bienvenido",
          "user": {
            "id": 5,
            "email": "test@gmail.com",
            "names": "Jose Alejandro",
            "surnames": "Peña Gonzalez",
            "role": "user",
            "type_register": "app",
            "auth_token": "3989617",
            "is_active": true
          }
        }
        run_test!
      end

      response '404', 'Email not found' do
        examples 'application/json' => {
          "success": false,
          "status": 404,
          "message": "El correo electrónico no existe"
        }
        run_test!
      end

      response '400', 'Invalid password' do
        examples 'application/json' => {
          "success": false,
          "status": 400,
          "message": "Contraseña incorrecta"
        }
        run_test!
      end

      response '403', 'User disable' do
        examples 'application/json' => {
          "success": false,
          "status": 403,
          "message": "Su cuenta se encuentra temporalmente inhabilitada por uso indebido"
        }
        run_test!
      end
    end
  end
end