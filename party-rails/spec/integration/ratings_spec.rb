# spec/integration/ratings_spec.rb
describe 'Users API' do

  path '/ratings' do

    post 'New rating' do
      tags 'Ratings'
      consumes 'application/json'
      parameter name: 'X-Auth-Token', in: :header, type: :string, required: true, description: "Session token"
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          user_id: { type: :string },
          user_service_id: { type: :string },
          score: { type: :string },
          comment: { type: :string }
        },
        required: [ 'user_id', 'user_service_id', 'score' ]
      }

      response '201', 'user created' do
        examples 'application/json' => {
          "success": true,
          "status": 201,
          "message": "Calificación exitosa"
        }
        run_test!
      end

      response '404', 'Request not exists' do
        examples 'application/json' => {
          "success": false,
          "status": 404,
          "message": "Servicio no existe"
        }
        run_test!
      end
    end
  end
end