# spec/integration/services_spec.rb
describe 'Services API' do

  path '/services' do

    get 'Get services' do
      tags 'Services'
      consumes 'application/json'
      parameter name: 'X-Auth-Token', in: :header, type: :string, required: true, description: "Session token"
      parameter name: 'user_id', in: :query, type: :integer, required: true, description: "Id user"
      
      response '200', 'services' do
        examples 'application/json' => {
          "success": true,
          "status": 200,
          "message": "Servicios ofertados",
          "services": [
            {
              "id": 4,
              "name": "TESTs",
              "description": "TESTs"
            }
          ]
        }
        # let(:services) { { id: 'Jose Alejandro', name: "TEST", description: "TEST" } }
        run_test!
      end
    end
  end

  path '/services' do

    post 'New resquest' do
      tags 'Services'
      consumes 'application/json'
      parameter name: 'X-Auth-Token', in: :header, type: :string, required: true, description: "Session token"
      parameter name: :parameters, in: :body, schema: {
        type: :object,
        properties: {
          user_id: { type: :integer },
          service_id: { type: :integer }
        },
        required: [ 'user_id', 'service_id' ]
      }
      
      response '201', 'Request created' do
        examples 'application/json' => {
          "success": true,
          "status": 201,
          "message": "Solicitud creada correctamente"
        }
        run_test!
      end

      response '404', 'Request not exists' do
        examples 'application/json' => {
          "success": false,
          "status": 404,
          "message": "Servicio no existe"
        }
        run_test!
      end
    end
  end
end