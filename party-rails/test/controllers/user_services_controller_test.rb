require 'test_helper'

class UserServicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_service = user_services(:one)
  end

  test "should get index" do
    get user_services_url
    assert_response :success
  end

  test "should get new" do
    get new_user_service_url
    assert_response :success
  end

  test "should create user_service" do
    assert_difference('UserService.count') do
      post user_services_url, params: { user_service: { service_id: @user_service.service_id, user_id: @user_service.user_id } }
    end

    assert_redirected_to user_service_url(UserService.last)
  end

  test "should show user_service" do
    get user_service_url(@user_service)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_service_url(@user_service)
    assert_response :success
  end

  test "should update user_service" do
    patch user_service_url(@user_service), params: { user_service: { service_id: @user_service.service_id, user_id: @user_service.user_id } }
    assert_redirected_to user_service_url(@user_service)
  end

  test "should destroy user_service" do
    assert_difference('UserService.count', -1) do
      delete user_service_url(@user_service)
    end

    assert_redirected_to user_services_url
  end
end
