require "application_system_test_case"

class UserServicesTest < ApplicationSystemTestCase
  setup do
    @user_service = user_services(:one)
  end

  test "visiting the index" do
    visit user_services_url
    assert_selector "h1", text: "User Services"
  end

  test "creating a User service" do
    visit user_services_url
    click_on "New User Service"

    fill_in "Service", with: @user_service.service_id
    fill_in "User", with: @user_service.user_id
    click_on "Create User service"

    assert_text "User service was successfully created"
    click_on "Back"
  end

  test "updating a User service" do
    visit user_services_url
    click_on "Edit", match: :first

    fill_in "Service", with: @user_service.service_id
    fill_in "User", with: @user_service.user_id
    click_on "Update User service"

    assert_text "User service was successfully updated"
    click_on "Back"
  end

  test "destroying a User service" do
    visit user_services_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "User service was successfully destroyed"
  end
end
