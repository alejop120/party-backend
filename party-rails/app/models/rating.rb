class Rating < ApplicationRecord
  belongs_to :user_service
  belongs_to :user

  validates :score, presence: true
end
