class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable


  has_many :user_services, dependent: :destroy
  has_many :services, through: :user_services

  enum role: [:admin, :staff, :user]
  enum type_register: [:web, :app]

  validates :email, presence: true
  validates :names, presence: true
  validates :surnames, presence: true
  validates :role, presence: true
  validates :type_register, presence: true
  validates :auth_token, presence: true, if: Proc.new { |user| user.user? }, uniqueness: true, allow_nil: true

  def as_json(options={})
    super(except: [:created_at, :updated_at])
  end
end
