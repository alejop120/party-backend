class Service < ApplicationRecord
	has_many :user_services, dependent: :destroy
	has_many :services, through: :user_services
	
	scope :actives, -> { where is_active: true }

	def as_json(options={})
		super({except: [:is_active, :created_at, :updated_at]})
	end
end
