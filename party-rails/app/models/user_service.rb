class UserService < ApplicationRecord
  belongs_to :user
  belongs_to :service
  has_many :users, dependent: :destroy
  has_many :staffs, through: :users
  has_many :ratings, dependent: :destroy

  def score_by_user
  	return ratings.find_by_user_id(user_id).score if ratings.find_by_user_id(user_id).present?
  	"Sin calificación aún"
  end

  def comment_by_user
  	return ratings.find_by_user_id(user_id).comment if ratings.find_by_user_id(user_id).present?
  	"Sin comentario"
  end
end
