# app/constraints/role_constraint.rb
class RoleConstraint
  def initialize(*roles)
    @roles = roles.map { |r| r.to_s }
  end

  def matches?(request)
    @roles.include? request.env['warden'].user(:user).try(:role)
  end
end