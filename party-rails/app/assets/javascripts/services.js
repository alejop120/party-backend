jQuery(function($) {
  $('#serviceModal').on('shown.bs.modal', function (event) {
    // autofocus first input form to modal bootstrap
    $(this).find('[autofocus]').focus();
  })
});

function validateServiceForm(){
  var response = true
  var spanAlert = document.getElementById("span_form");
  var incompleteFields = "Campos incompletos";

  var nameInput = document.getElementById("service_name");
  if (nameInput.value) {
    nameInput.style.borderColor = "#ccc";
  }else{
    response = false;
    nameInput.style.borderColor = "red";
    spanAlert.innerHTML = incompleteFields;
  }
  var descriptionInput = document.getElementById("service_description");
  if (descriptionInput.value) {
    descriptionInput.style.borderColor = "#ccc";
  }else{
    response = false;
    descriptionInput.style.borderColor = "red";
    spanAlert.innerHTML = incompleteFields;
  }
  return response;
}

function dismissServiceModal(){
  $("#content-service-modal").empty();
  $('#serviceModal').modal('hide');
}
