// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require popper
//= require bootstrap
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .
//= require data-confirm-modal

dataConfirmModal.setDefaults({
  title: 'Esta seguro ?',
  commit: 'Confirmar',
  cancel: 'Cancelar'
});

function validateFormLogin() {
  var response = true
  var spanAlert = document.getElementById('alertLogin');
  var incompleteFields = 'Campos incompletos'

  var emailInput = document.getElementById('user_email');
  if (!emailInput.value) {
    spanAlert.innerHTML = incompleteFields;
    response = false;
  }

  var passwordInput = document.getElementById('user_password');
  if (!passwordInput.value) {
    spanAlert.innerHTML = incompleteFields;
    response = false;
  }
  return response;
}


$(document).ready(function () {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar, #content').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
});