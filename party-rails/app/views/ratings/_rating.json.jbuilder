json.extract! rating, :id, :score, :comment, :user_service_id, :user_id, :created_at, :updated_at
json.url rating_url(rating, format: :json)
