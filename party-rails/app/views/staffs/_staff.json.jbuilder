json.extract! staff, :id, :user_service_id, :user_id, :created_at, :updated_at
json.url staff_url(staff, format: :json)
