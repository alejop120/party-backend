module ApplicationHelper

	def is_active_controller(controller_name)
		controller_name.include?(params[:controller]) ? "active" : nil
	end

	def yes_no(value)
		if value
			'<span class="badge badge-success">SI</span>'.html_safe
		else
			'<span class="badge badge-danger">NO</span>'.html_safe
		end
	end
	
end
