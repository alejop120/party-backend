module Api::V1::Concerns
  module Authorization

    extend ActiveSupport::Concern

    included do
      before_action :auth, except: [:register, :login]
    end

    protected

      def auth
        result = { success: false, status: 403, message: "Invalid Token", user_message: "Sesión finalizada, alguien inició sesión con tu cuenta en otro dispositivo"}
        if request.headers["X-Auth-Token"].blank?
          result[:message] = 'Auth Token required'
          render json: result
        else
          if params[:user_id].blank?
            result[:message] = "user_id required"
            render json: result
          else
            @current_user = User.find_by(id: params[:user_id], auth_token: request.headers["X-Auth-Token"])
            if @current_user.present? && @current_user.is_active == false
              result[:message] = "Su cuenta se encuentra temporalmente inhabilitada por uso indebido"
              result[:user_message] = "Su cuenta se encuentra temporalmente inhabilitada por uso indebido"
              render json: result
            elsif @current_user.blank?
              render json: result
            end
          end
        end
      end

  end
end