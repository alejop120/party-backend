module Api::V1::Concerns
  module SkipAuthenticate
    extend ActiveSupport::Concern

    included do
  	  skip_before_action :authenticate_user!
    end
    
  end
end