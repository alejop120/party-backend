module Api::V1::Concerns
	module GenerateUserToken

		extend ActiveSupport::Concern

		def generate_auth_token
			token_exists = true
			while token_exists == true do
			  auth_token = rand.to_s[2..8]
			  token_exists = User.where(auth_token: auth_token).present?
			end
			return auth_token
		end

	end
end