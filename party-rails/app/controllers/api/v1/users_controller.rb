module Api::V1
	class UsersController < ApplicationController

		include Concerns::SkipAuthenticate
    	include Concerns::Authorization
    	include Concerns::GenerateUserToken

		
		def register
			result = { success: false, status: 400, message: "Bienvenido" }
			json_schema = "app/controllers/api/v1/schemas/users/register.json"
			begin
				JSON::Validator.validate!(json_schema, params.to_json)
				type_register = params[:type_register]
				if ["app"].include? type_register
					email = params[:email]
					names = params[:names]
					surnames = params[:surnames]
					password = params[:password]
					device_token = params[:device_token]
					if password.present?
						password_confirmation = params[:password_confirmation]
						if password == password_confirmation
							if password.length > 5
								validate_email = User.where(email: email)
								if validate_email.present?
									result[:status] = 409
									result[:message] = "El correo electrónico ya está registrado"
								else
									user = User.new(email: email, names: names, surnames: surnames, type_register: type_register, auth_token: generate_auth_token(), password: password)
									if user.save
										result[:success] = true
										result[:status] = 201
										result[:message] = "Bienvenido"
										result[:user] = user
									else
										result[:message] = "Error al crear usuario: #{user.errors.full_messages}"
									end
								end
							else
								result[:message] = "La contraseña debe ser mínimo de 6 caracteres"
							end
						else
							result[:message] = "La contraseña no coincide"
						end
					else
						result[:message] = "Contraseña obligatoria"
					end
				else
					result[:status] = 422
					result[:message] = "Tipo de registro inválido"
				end
			rescue JSON::Schema::ValidationError => e
				result[:message] = e.message
			end

			render json: result
		end

		def login
			result = { success: false, status: 400, message: "Bienvenido" }
			json_schema = "app/controllers/api/v1/schemas/users/login.json"
			begin
			  JSON::Validator.validate!(json_schema, params.to_json)
			  user = User.where(email: params[:email], role: 2)
			  if user.present?
			  	if user.first.valid_password?(params[:password])
			  		if user.first.is_active == false
			  			result[:status] = 403
			  			result[:message] = "Su cuenta se encuentra temporalmente inhabilitada por uso indebido"
			  		else
			  			if user.first.update(auth_token: generate_auth_token())
			  				result[:success] = true
			  				result[:status] = 200
			  				result[:user] = user.first
			  			else
			  				result[:status] = 500
			  				result[:message] = "Error al asignar token de sesion: #{user.first.errors.full_messages}"
			  			end
			  		end
			  	else
			  		result[:message] = "Contraseña incorrecta"
			  	end
			  else
			  	result[:status] = 404
			  	result[:message] = "El correo electrónico no existe"
			  end
			rescue JSON::Schema::ValidationError => e
			  result[:message] = e.message
			end
			render json: result
		end

	end
end