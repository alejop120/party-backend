module Api::V1
	class ServicesController < ApplicationController

		include Concerns::SkipAuthenticate
    	include Concerns::Authorization

		
		def index
			result = { success: true, status: 200, message: "Servicios ofertados" }
			result[:services] = Service.actives
			render json: result
		end

		def new_request
			result = { success: false, status: 400, message: "Solicitud creada correctamente" }
			json_schema = "app/controllers/api/v1/schemas/services/new_request.json"
			begin
				JSON::Validator.validate!(json_schema, params.to_json)

				service_id = params[:service_id]
				service = Service.find_by_id(service_id)

				if service.present?
					new_user_service = UserService.new(service_id: service_id, user_id: @current_user.id)
					if new_user_service.save
						result[:success] = true
						result[:status] = 201
					else
						result[:message] = "Error al registrar solicitud: #{new_user_service.errors.full_messages}"
					end
				else
					result[:status] = 404
					result[:message] = "Servicio no existe"
				end
			rescue JSON::Schema::ValidationError => e
				result[:message] = e.message
			end

			render json: result
		end

	end
end