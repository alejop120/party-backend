module Api::V1
	class RatingsController < ApplicationController

		include Concerns::SkipAuthenticate
    	include Concerns::Authorization

		
		def new_rating
			result = { success: false, status: 400, message: "Calificación exitosa" }
			json_schema = "app/controllers/api/v1/schemas/ratings/new_rating.json"
			begin
				JSON::Validator.validate!(json_schema, params.to_json)

				user_service_id = params[:user_service_id]
				user_service = UserService.find_by_id(user_service_id)

				if user_service.present?
					if user_service.user_id == @current_user.id
						new_rating = Rating.new(user_id: @current_user.id, user_service_id: user_service_id, score: params[:score], comment: params[:comment])
						if new_rating.save
							result[:success] = true
							result[:status] = 201
						else
							result[:message] = "Error al registrar calificación: #{new_rating.errors.full_messages}"
						end
					else
						result[:message] = "Servicio no pertenece a usuario calificador"
					end
				else
					result[:status] = 404
					result[:message] = "Servicio no existe"
				end
			rescue JSON::Schema::ValidationError => e
				result[:message] = e.message
			end

			render json: result
		end

	end
end