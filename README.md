Administrable (role :admin):
http://party.fidelizapp.com.co
El acceso del administrable es admin@party.com.co y contraseña 12345678, el administrable permite los siguiente:
1. Crear servicios para ofertar y ser vizualizados al usuario final (por medio del api)
2. Ver servicios solicitados de cada servicio ofertado y calificaciones del mismo por parte del usuario final (role :user)


API:
El Api consta con los siguientes servicios web que reciben un token de sesión único en el header para verificar identidad del usuario.
1. Registro de usuario ({{domain}}/api/v1/users [POST])
2. Login ({{domain}}/users/login [POST])
3. Ver servicios ofertados para el usuario final ({{domain}}/services [GET])
4. Adquirir nuevo servicio ofertado ({{domain}}/services [POST])
5. Calificar servicio por parte del usuario final ({{domain}}/ratings [POST])

Nota: el auth_token (token para consumir WS) se obtiene del objeto 'user' al realizar un registro exitoso y se pasa como parámetro header llamado 'X-Auth-Token'.

Documentación del API:
http://party.fidelizapp.com.co/api-docs/index.html
